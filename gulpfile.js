var gulp = require("gulp");
var babelify = require('babelify');
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var streamify = require('gulp-streamify');
var source = require('vinyl-source-stream');
var streamify = require('gulp-streamify');
var connect = require('gulp-connect');

gulp.task('browserify', function() {
    browserify( 'src/main.jsx', { debug: false })
        .transform('babelify', {presets: ['es2015', 'react'], ignore: /(bower_components)|(node_modules)/})
        .bundle()
        .on("error", function (err) { console.log("Error : " + err.message); })
        .pipe(source('bundle.js'))
        .pipe(streamify(uglify()))
        .pipe(gulp.dest('./dist'))
        .pipe(connect.reload());
});

gulp.task("watch", ['browserify'], function(){
    gulp.watch('src/*.jsx', ['browserify'])
});

gulp.task('connect', function() {
  connect.server({
    root: 'dist',
    livereload: true
  });
});

gulp.task('default', ['connect', 'watch'])

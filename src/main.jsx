// React をロード
import React from 'react';
import ReactDOM from 'react-dom';
import Message from './message.jsx';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      person: {
        name: 'ヤマダ',
        age: 34
      }
    }
  }
  handleChange(event) {
    this.setState({
      person: {
        name: event.target.value,
        age: this.state.person.age
      }
    });
  }
  render() {
    return (
      <div>
        <input type="text" value={this.state.person.name} onChange={this.handleChange.bind(this)} />
        <Message name={this.state.person.name} age={this.state.person.age} />
      </div>
    );
  }
}

// app クラスを描画
ReactDOM.render(<App />, app);

//http://www.tamas.io/react-with-es6/
//http://ilikekillnerds.com/2015/02/developing-react-js-components-using-es6/
//https://www.youtube.com/watch?v=q8MmvQsq-FM

// React をロード
import React from 'react';

// Message クラス
class Message extends React.Component {
  render() {
    return (
      <p>
        こんにちは。{this.props.name} さん {this.props.age} 歳ですね！
      </p>
    );
  }
}

// エクスポート
export default Message;
